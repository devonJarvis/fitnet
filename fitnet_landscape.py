import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import time
from functools import partial
import jax
from jax import jit, grad, random
from jax.flatten_util import ravel_pytree
from jax.experimental import optimizers
from jax.experimental import stax
from jax.experimental.stax import (AvgPool, BatchNorm, Conv, Dense, Dropout, FanInSum,                                                       
                                   FanOut, Flatten, GeneralConv, Identity,                                                                   
                                   MaxPool, Tanh, Relu, LogSoftmax)
from jax.nn.initializers import variance_scaling, normal, glorot_normal
import jax.numpy as jnp
import datasets
import wandb
import log_handler

def add_two_trees_selective_l2(grad_tree, l2_tree, selection_tree, max_tree):                                #0.05 
    return step_size*grad_tree + selective_l2_step*l2_tree*(1 - (jnp.abs(selection_tree)>select_rate*max_tree))

padding_set = [[1,1], [1,1]]

# Architecture Definition
def ResNet50(num_classes):
  return stax.serial(                                                           #scaled_glorot_small
      GeneralConv(('HWCN', 'OIHW', 'NHWC'),32, (3, 3), (1, 1), padding=padding_set, W_init = scaled_glorot_large()),Relu,
      Conv(32, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(32, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(48, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(48, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      MaxPool((2, 2), strides=(2, 2)),
      Conv(80, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(80, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(80, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(80, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(80, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      MaxPool((2, 2), strides=(2, 2)),
      Conv(128, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(128, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(128, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(128, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      Conv(128, (3, 3), W_init = scaled_glorot_large(), padding=padding_set), Relu,
      MaxPool((8, 8), strides=(2, 2)),
      Flatten, Dense(500, W_init = scaled_glorot_large()), Relu,
      Dense(num_classes, W_init = scaled_glorot_large()),
      LogSoftmax)

@jit
def accuracy(params, batch):
  inputs, targets = batch
  target_class = jnp.argmax(targets, axis=-1)
  predicted_class = jnp.argmax(predict_fun(params, inputs), axis=-1)
  return jnp.mean(predicted_class == target_class)

# Functions for the different possible pieces of a loss function
@jit
def loss(params, batch):
  inputs, targets = batch
  preds = predict_fun(params, inputs)
  return -jnp.mean(jnp.sum(preds * targets, axis=1))

@jit
def mahalanobis_dist(params, batch, params_constant_ravel):
  inputs, targets = batch
  grads = grad(loss)(params, batch)
  grads_flat, _ = ravel_pytree(grads)
  return jnp.dot(params_constant_ravel.T, grads_flat)

@jit
def l2_regularizer(params):
  flat_params, unflatten = ravel_pytree(params)
  return jnp.dot(flat_params.T, flat_params)

# Run experiments
if __name__ == "__main__":
  wandb.init(project = 'L2 Landscape')

  # Set hyper-parameters
  wandb.config.param_scale_regime_2 = 0.2 #0.2
  wandb.config.param_scale_regime_3 = 3.0 #3.0 #5.0
  wandb.config.step_size = 0.01 #0.005 #0.001 #0.01
  wandb.config.num_epochs = 200 #700 #1000 #400 #250 #200 #150 #50
  wandb.config.batch_size = 32 #32 #128
  wandb.config.num_classes = 10
  wandb.config.input_shape = (32, 32, 3, wandb.config.batch_size)
  wandb.config.num_trainings = 5 #10 #3
  wandb.config.reg_rates = [1e-6,5e-6,1e-5,5e-5,1e-4,5e-4] 
  wandb.config.selection_rates = [0.0,0.02,0.1,0.2,0.4,1.0]
  training_names_start = str(np.char.array(list(wandb.config.reg_rates))[:,None] + np.array(['|']).astype('|S6')\
                   + np.char.array(list(wandb.config.selection_rates))).replace('[','').replace(']','').replace('b','')\
                   .replace('\n','').replace('\"','').split(' ')
  training_names = []
  for item in training_names_start:
      if not item == '':
          training_names.append(item)
  training_names = np.array(training_names).reshape(len(wandb.config.reg_rates), len(wandb.config.selection_rates))


  init_seeds_to_start = np.random.choice(100000000, wandb.config.num_trainings, replace=False).astype(np.uint32)
  if wandb.config.num_trainings == 1:
      wandb.config.init_seeds = np.array([init_seeds_to_start])
  else:
      wandb.config.init_seeds = init_seeds_to_start

  scaled_glorot_small = partial(variance_scaling, wandb.config.param_scale_regime_2, "fan_avg", "truncated_normal")#0.2 normal 5.0 overfit
  scaled_glorot_large = partial(variance_scaling, wandb.config.param_scale_regime_3, "fan_avg", "truncated_normal")

  # Function to use our architecture
  init_fun, predict_fun = ResNet50(wandb.config.num_classes)
  opt_init, opt_update, get_params = optimizers.sgd(1.0)          #(wandb.config.step_size)

  # Loading the dataset
  num_batches, batches, track_batch, test_batches = datasets.cifar10_loaders(wandb.config.batch_size)

  # Create log file and arrays to track the repeated errors
  train_logs = np.zeros(wandb.config.num_epochs)
  test_logs = np.zeros(wandb.config.num_epochs)
  norm_logs = np.zeros(wandb.config.num_epochs)

  print(training_names)

  for reg_rate_idx in range(len(wandb.config.reg_rates)):
    for selection_rate_idx in range(len(wandb.config.selection_rates)):

        @jit
        def update_Selective(opt_state, batch, epoch):#, init_Q):
            params = get_params(opt_state)
            const_flat_params, unflattener = ravel_pytree(params)
            important_params = grad(mahalanobis_dist)(params, batch, const_flat_params)
            max_params = important_params.copy()
            for i in [0,2,4,6,8,11,13,15,17,19,22,24,26,28,30,34,36]: #[0,3,6,8,10,12,14]:
                max_params[i] = tuple([jnp.ones(max_params[i][0].shape)*jnp.max(max_params[i][0]),\
                              jnp.ones(max_params[i][1].shape)*jnp.max(max_params[i][1])])
            grads = grad(loss)(params, batch)
            reg_grads = grad(l2_regularizer)(params)
            full_grads = jax.tree_util.tree_multimap(add_two_trees_selective_l2, grads, reg_grads, important_params, max_params)
            return opt_update(epoch, full_grads, opt_state)

        
        for seed in init_seeds_to_start:
          print("############### New "+training_names[reg_rate_idx][selection_rate_idx]+" Training ########################")
          rng_key = random.PRNGKey(seed)
          _, init_params = init_fun(rng_key, wandb.config.input_shape)

          step_size = wandb.config.step_size
          selective_l2_step = wandb.config.reg_rates[reg_rate_idx]
          select_rate = wandb.config.selection_rates[selection_rate_idx]
          print("Setp Size: ", step_size)
          print("Reg Rate: ", selective_l2_step)
          print("Selection Rate: ", select_rate)

          opt_state = opt_init(init_params)
          run_logs =  log_handler.init_run_logs() # training, test, time logs, weight norm
          for epoch in range(wandb.config.num_epochs):
              start_time = time.time()
              for _ in range(num_batches):
                opt_state = update_Selective(opt_state, next(batches), epoch)
              epoch_time = time.time() - start_time
              params = get_params(opt_state)
              norm = np.sum([np.linalg.norm(params[index][0])+np.linalg.norm(params[index][1]) 
                       for index in [0,2,4,6,8,11,13,15,17,19,22,24,26,28,30,34,36]])

              train_acc = accuracy(params, next(track_batch))
              test_acc = accuracy(params, next(test_batches))
              print("Epoch {} in {:0.2f} sec".format(epoch, epoch_time))
              print("Training set accuracy {}".format(train_acc))
              print("Test set accuracy {}".format(test_acc))
              wandb.log({"epoch": epoch, "epoch_time": epoch_time,\
                         training_names[reg_rate_idx][selection_rate_idx]+" Train Accuracy": float(train_acc),\
                         training_names[reg_rate_idx][selection_rate_idx]+" Test Accuracy": float(test_acc)})

              run_logs[0] = np.append(run_logs[0], train_acc)
              run_logs[1] = np.append(run_logs[1], test_acc)
              run_logs[3] = np.append(run_logs[3], norm)

          train_logs = np.vstack([train_logs, run_logs[0].reshape(1,wandb.config.num_epochs)])
          test_logs = np.vstack([test_logs, run_logs[1].reshape(1,wandb.config.num_epochs)])
          norm_logs = np.vstack([norm_logs, run_logs[3].reshape(1,wandb.config.num_epochs)])

  train_logs = train_logs[1:]
  test_logs = test_logs[1:]
  norm_logs = norm_logs[1:]

  train_file = open('train_file.txt', 'w')
  train_file.write(str(train_logs))
  train_file.close()

  test_file = open('test_file.txt', 'w')
  test_file.write(str(test_logs))
  test_file.close()

  norm_file = open('norm_file.txt', 'w')
  norm_file.write(str(norm_logs))
  norm_file.close()

  ave_trains = np.array([np.mean(train_logs[i: i+wandb.config.num_trainings],axis=0)\
                                 for i in range(0,train_logs.shape[0],wandb.config.num_trainings)])
  ave_tests = np.array([np.mean(test_logs[i: i+wandb.config.num_trainings],axis=0)\
                                 for i in range(0,train_logs.shape[0],wandb.config.num_trainings)])
  ave_norms = np.array([np.mean(norm_logs[i: i+wandb.config.num_trainings],axis=0)\
                                 for i in range(0,train_logs.shape[0],wandb.config.num_trainings)])

  ave_train_test = 0.5*(ave_trains+ave_tests)
  best_index = np.argmax(ave_train_test, axis=1)

  train_maxes = ave_trains[np.arange(ave_trains.shape[0]),best_index]
  test_maxes = ave_tests[np.arange(ave_tests.shape[0]),best_index]
  final_norms = norm_logs[:,-1]

  train_file = open('train_max_file.txt', 'w')
  train_file.write(str(train_maxes))
  train_file.close()

  test_file = open('test_max_file.txt', 'w')
  test_file.write(str(test_maxes))
  test_file.close()

  norm_file = open('norm_final_file.txt', 'w')
  norm_file.write(str(final_norms))
  norm_file.close()

